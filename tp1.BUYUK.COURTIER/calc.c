#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/**** TP BUYUK Baran & COURTIER Antoine 
	TP de PROG 1 GROUPE TP5 PROG 3 ****/


int add( int a, int b){
	return a+b;
}
int sous(int a , int b){
	return a-b;
}
int divi(int a, int b){
	return b!=0?a/b:0;
}
int mul(int a, int b){
	return a*b;
}

int main(int argc, const char *argv[])
{
	
	if(argc != 4){
		printf("Nombre d'arguments insuffisant (outils dispo +,-,x,/)\n");
		return 0;
	}

	int a=atoi(argv[2]) , b=atoi(argv[3]);


	if( strcmp(argv[1],"+\0") == 0)
		printf("%d \n",add(a,b));
	else if( strcmp(argv[1],"-\0") == 0)
		printf("%d \n",sous(a,b));
	else if( strcmp(argv[1],"/\0") == 0)
		printf("%d \n",divi(a,b));
	else if( strcmp(argv[1],"x\0") == 0)
		printf("%d \n",mul(a,b));
	else
		printf("Erreurs d'arguments ( outils dispo +,-,x,/)\n");
		
			 
	
		
	return 0;
}
