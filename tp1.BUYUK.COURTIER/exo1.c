#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/**** TP BUYUK Baran & COURTIER Antoine 
	TP de PROG 1 GROUPE TP5 PROG 3 ****/

/* strcooll nous compare la longueur des deux chaines de caracteres */

int s_strcoll(char *s1 , char *s2){
	
	/* le pointeur ne doit pas etre nul */

	return strcoll(s1,s2);
}

/* si le caractere c est bien la premiere lettre de la chaine de caractere*/
char *s_strchr(char *s1 , char c){

	return strchr(s1,c);
}

/* atoi convertie la chaine de caractere qui bien sur est un nombre aussi dans le string en un nombre  
 ex : cdc: 655 atoi renvoie un int de 655 */

int s_atoi(char *s1){

	return atoi(s1);
}

int main(int argc, const char *argv[])
{
	char s1[12]={"lalalal"};
	char s2[10]={"lalalal"};
	char s3[5]={"123"};

	
	printf("%d\n",s_strcoll(s1,s2));
	printf("%s\n",s_strchr(s1,'l')!=NULL?s_strchr(s1,'l'):"ERREUR");
	printf("%d\n",s_atoi(s3));
	return 0;
}
