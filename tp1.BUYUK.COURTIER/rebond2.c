#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <MLV/MLV_all.h>

#define VIT 100000 //regle la vitesse du cube


/**** TP BUYUK Baran & COURTIER Antoine 
	TP de PROG 1 GROUPE TP5 PROG 3 ****/

/*
	gcc -o f_sortie f_entree -Wextra -ansi -lMLV
	 echap pour commencer et entree pour finir
 */


int main(int argc, const char *argv[])
{
	srand(time(NULL));
	MLV_Keyboard_button touche;
	MLV_create_window(" Test " , "Test" , 600 , 400);
	
	MLV_draw_filled_rectangle(50,50, 300, 300, MLV_COLOR_RED);
	MLV_draw_filled_rectangle(150 , 150 , 50 , 50 , MLV_COLOR_BLUE);
	MLV_actualise_window();
	
	MLV_Event event;
			
	int a=100,c=100;
	int h=1,b=0;
	
	/* en attente du premier entree / Appuyez sur echap */
	do{
		MLV_wait_keyboard(&touche,NULL ,NULL);
	}while( touche != MLV_KEYBOARD_ESCAPE);
	touche= MLV_KEYBOARD_NONE;

	/* on bouge le carré jusqu'au prochain entrée */
	do{
		
		if( (a+50) >= 350){
			h=0;
			b=rand()%2;
		}
		if( a <= 50){
			h=1;
			b=rand()%2;
		}
		if( (c+50) >= 350){
			b=0;
			h=rand()%2;
		}
		if( c <= 50){
			b=1;
			h=rand()%2;
		}
		

		if( h==1)
			a=a+50;
		else
			a=a-50;
		if( b==1)
			c=c+50;
		else
			c=c-50;
		
			
		MLV_draw_filled_rectangle(0,0,640,480,MLV_COLOR_BLACK);
		MLV_draw_filled_rectangle(50,50,300,300,MLV_COLOR_RED);
		MLV_draw_filled_rectangle(a,c,50,50,MLV_COLOR_BLUE);
		MLV_actualise_window();
		
		event = MLV_get_event(&touche,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
		usleep(VIT);

	}while( touche != MLV_KEYBOARD_RETURN);


	return 0;
}
