#include <stdio.h>
#include <stdlib.h>
#include <string.h>
	

/**** TP BUYUK Baran & COURTIER Antoine 
	TP de PROG 1 GROUPE TP5 PROG 3 ****/


int bonModulo(int a , int b){

	return a%b<0?(a%b)*(-1):(a%b);
}

void chiffre(int key, char* mot){
	
	int nb_carac=strlen(mot);
	int i;
		
	for ( i = 0; i < nb_carac; i++) {
		/* code */
		if( mot[i] >= 65 && mot[i] <= 90){
			/* pour les lettres majuscules */
			if( mot[i]+key > 90)
				mot[i]=mot[i]+key-90+65;
			else
			        mot[i]=mot[i]+key;	
			
		}
		else if( mot[i] >= 97 && mot[i] <= 122){
			/* pour les minuscules */
			if( mot[i]+key > 122 )
				mot[i]=mot[i]+key-90+65;
			else
				mot[i]=mot[i]+key;
		}
	}
	
	printf("%s \n",mot);
}

void dechiffre(int key, char* mot){
	
	int nb_carac=strlen(mot);
	int i;
		
	for ( i = 0; i < nb_carac; i++) {
		/* code */
		if( mot[i] >= 65 && mot[i] <= 90){
			/* pour les lettres majuscules */
			if( mot[i]-key < 65)
				mot[i]=mot[i]-key-90+65;
			else
			        mot[i]=mot[i]-key;	
			
		}
		else if( mot[i] >= 97 && mot[i] <= 122){
			/* pour les minuscules */
			if( mot[i]-key > 122 )
				mot[i]=mot[i]-key-90+65;
			else
				mot[i]=mot[i]-key;
		}
	}
	
	printf("%s \n",mot);
}



int main(int argc, const char *argv[])
{
	char s[100]="Les TP de programmation sont super !";
		printf("%s \n",s);
		chiffre(3,s);
		dechiffre(3,s);	
	return 0;
}
