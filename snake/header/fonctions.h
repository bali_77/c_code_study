#ifndef fonctions
#define fonctions
    
/* entrer le nom du header au dessus */
/** LIB INCLUDES **/

#include "structures.h"
/******************/

/* Prototypes */
/* Prototypes des fonctions */
/* initialise le tableau entré en parametre en ajoutant la nourriture sur la map */
void init_damier(Case damier[][MAX]);

/* Affiche le jeu avec la mlv*/
void affichage();
/* Verifie l'etat du damier */
void gardien();
/* Verifie si on a gagné ou perdu */
void win(int what);
/* mange si possible la nourriture */
void eat();
/* augmente le corps du serpent */
void corps( int x , int y);
/* fonction qui gere la direction du serpent */
void up();
void down();
void left();
void right();
/* Le jeu se passe ici / controle au clavier etc */
void mlv();
/* initialise le serpent */
void init_snake();
/* lance le jeu */
void launch();

/***************************/


/*************/

#endif
