#include "header/fonctions.h"

void init_damier(Case damier[][MAX]){

    int i, j ;
    int a , b;
    bouffe_restant=0;
    /* il y au maximum 40 cases à manger */
    for(i=0; i<(MAX) ; i++){
        a = rand()%MAX;
        b = rand()%MAX;
        if(damier[a][b].bouffe == 0){
        damier[a][b].bouffe=1;
        bouffe_restant++;
        }
        else
            i--;
    }

    for(i=0 ; i < MAX ; i ++)
        for(j=0 ; j < MAX ; j++){
           damier[i][j].presence =0;
        }

    printf("Initialisation du DAMIER OK !!! \n"); 
}

void affichage(){
    float x = (REX/MAX);
    float y = (REY/MAX);
    float i, j;
    int k=0 , l=0 ;
 
    /* affiche le serpent et la nouriture */
    for(i=0; i < (REY) ; i += y, k++ , l=0)
        for(j=0; j < (REX) ; j += x , l++){
            if( damier[k][l].bouffe == 1){
                MLV_draw_filled_rectangle( j, i , x , y , MLV_COLOR_RED);
            }
            else
                MLV_draw_filled_rectangle(j ,i , x , y , MLV_COLOR_BLACK);
    
            if(damier[k][l].presence > 0 ){
                if( damier[k][l].tete == 0){
                MLV_draw_filled_rectangle(j, i , x , y , MLV_COLOR_GREEN);
                }
                else{
                    MLV_draw_filled_rectangle(j,i,x,y, MLV_COLOR_WHITE);
                }
            }
               
        }

        MLV_actualise_window();
        
  

}

/* enleve -1 à la presence si elle existe */
void gardien(){
/* -1 si le corps du serpent est present */
   int i , j;

    for(i=0 ; i<MAX; i++)
        for(j=0; j<MAX ; j++){
            if( damier[i][j].tete== 1)
                damier[i][j].tete=0;
            if(damier[i][j].presence > 0 )
                damier[i][j].presence -= 1;
        }
}

/*fonction de fin */
void win(int what){
      
    MLV_Image *win , *lost ;

    win = MLV_load_image("win.gif");
    lost = MLV_load_image("lost.gif");

    MLV_resize_image_with_proportions(win,REX,REY);
    MLV_resize_image_with_proportions(lost,REX,REY);

        if(what == 0){
            printf("You lost \n");
            MLV_draw_image(lost, 0, 0 );
            MLV_actualise_window();
            sleep(5);
            MLV_free_image(lost);
            MLV_free_image(win);
            exit(EXIT_FAILURE);
        }
        else{
            printf("You win \n");
            MLV_draw_image(win , 0 , 0);
            MLV_actualise_window();
            sleep(5);
            MLV_free_image(lost);
            MLV_free_image(win);
            exit(EXIT_SUCCESS);
        }
}
/* fonction manger */
void eat(){
/* augmente le corps du serpent et finie le jeu */
    if(damier[deb_x][deb_y].bouffe == 1){
        snake.corps += 1;
        bouffe_restant--;
        damier[deb_x][deb_y].bouffe = 0;
    }
        if( bouffe_restant == 0)
            win(1);
    
}

/* controle si le serpent ne se mange pas lui meme */
void corps( int x , int y){

    if(damier[x][y].presence > 0)
        win(0);
}

/* Regle les parametres du jeu , tout par d'une de ses quatres fonctions */
void up(){

           if((deb_x-1) < 0 ){
               deb_x= MAX;
            }
            deb_x -= 1;
            corps(deb_x,deb_y);
            eat();
            gardien();
            damier[deb_x][deb_y].presence += snake.corps;
            damier[deb_x][deb_y].tete=1;
}

void down(){

                         if((deb_x+1) == MAX){
                            deb_x= -1;
                            }
                          deb_x += 1;
                         corps(deb_x,deb_y);
                         eat();
                         gardien();
                         damier[deb_x][deb_y].presence += snake.corps;
            damier[deb_x][deb_y].tete=1;

}

void left(){
                            if((deb_y-1) < 0){
                            deb_y= MAX;
                            }
                            deb_y -= 1;
                             corps(deb_x,deb_y);
                             eat();
                             gardien();
                       damier[deb_x][deb_y].presence += snake.corps;
            damier[deb_x][deb_y].tete=1;

}

void right(){
                              if((deb_y+1) == MAX){
                              deb_y = -1;
                              }
                              deb_y += 1;
                            corps(deb_x,deb_y);
                            eat();
                            gardien();
                       damier[deb_x][deb_y].presence += snake.corps;
            damier[deb_x][deb_y].tete=1;

} 


/* tout se passe ici */
void mlv(){
    
    /* var */
    int touche = 3;
    int derniere_touche=touche; /* bloque le double appuie */
    /*Evenements clavier */
    MLV_Keyboard_button sym=-1;   /* Symbole appuyer */
    MLV_Keyboard_modifier mod=-1;       /* état du bouton appuyer inutile ? */
    MLV_Button_state state=-1;
    MLV_Event event= MLV_NONE;

       
    affichage();

    printf("Premier affichage OK !!! \n");
      
     /* traitement evenement */
        
     do{
      /* recupere l'évenement */
     event = MLV_get_event( &sym , &mod , NULL , NULL, NULL, NULL, NULL , NULL, &state);
    

     /* haut = x -1 ; bas x + 1 ; droite y+1 ; gauche y-1;*/
     /* touche 0 H , 1 B , 2 L , 3 R */
    switch(event){
            case MLV_NONE:
                          if(touche == 0)
                            up();
                          else if(touche == 1)
                            down();
                          else if(touche == 2)
                            left();
                          else if(touche == 3)
                            right();
                          
                            usleep(DIF);
                            derniere_touche=touche;
                            affichage();
            break;
            case MLV_KEY: /* action clavier */
            if( state == MLV_PRESSED){
                    if( sym == MLV_KEYBOARD_UP && derniere_touche != 0){

                        /* bloque le retour */
                        if( derniere_touche != 1){
                        up();
                        touche = 0;
                        }
                    }
                    else if ( sym == MLV_KEYBOARD_DOWN && derniere_touche != 1){

                        if( derniere_touche != 0){
                        down();
                        touche = 1;
                        }
                     }
                    else if ( sym == MLV_KEYBOARD_LEFT && derniere_touche != 2){
                      
                        if( derniere_touche != 3){
                        left();
                        touche = 2;
                        }
                   }
                    else if ( sym == MLV_KEYBOARD_RIGHT && derniere_touche != 3){
                        
                        if( derniere_touche != 2){
                        right();
                        touche = 3;
                        }
                   }
           
            }

      }                   
     }while(sym != MLV_KEYBOARD_SPACE);      


}


void init_snake(){
    int i , c , ok=0;

    do{
        i=rand()%MAX;
        c=rand()%MAX;
        if(damier[i][c].bouffe == 0){
            damier[i][c].presence=1;
            deb_x=i;
            deb_y=c;
            ok = 1;
        }
    }while( !ok);

}

void launch(){
/* le lanceur */     
    MLV_create_window("SNAKE V0.1","SNAKE V0.1",REX,REY);
    init_damier(damier);
      /* init snake */
      snake.corps =1;
      init_snake();

   mlv();
    
}


