#include"headers/position_bit.h"

struct _position{
  unsigned long long int tab;
};

int est_case_occupee(Position *pos, Case c){
  /*uLL = unsigned long long*/
  if((pos->tab & (1uLL << c)) > 0){
    return 1;
  }
  return 0;
}

Position* creer_position(){
  Position *pos;

  if( (pos = (Position*) malloc(sizeof(Position))) == NULL){
    exit(1);
  }
  pos->tab = 0;
  return pos;
}

void ajoute_position(Position *pos, Case c){
  pos->tab = pos->tab | (1uLL << c);
}

Position* calculer_cases_attaquees_dame(Case c){
  Position *pos = creer_position();
  Case c1, c2, c3, c4;
  int i;

  for(i=0, c1=c-c%8, c2=c%8, c3=c-(c%8)*9, c4=c+(c%8)*7; i<=7; i++, c1++, c2=c2+
	8, c3=c3+9, c4=c4-7){
    if(c1 != c){
      ajoute_position(pos, c1);
    }
    if(c2 != c){
      ajoute_position(pos, c2);
    }
    if(c3 >= 0 && c3 < 64 && c3 != c){
      /*Empêche de continuer la diagonale si elle a atteint un bord*/
      if(i != 0 && c3 % 8 < (c3 - 9) % 8){
	c3 = 64;
      }
      else{
	ajoute_position(pos, c3);
      }
    }
    if(c4 >= 0 && c4 < 64 && c4 != c){
      ajoute_position(pos, c4);
      /*Empêche de continuer la diagonale si elle a atteint un bord*/
      if(i != 0 && c4 % 8 > (c4 - 7) % 8){
	c4 = -1;
      }
    }
  }

  return pos;
}

int compare_position(Position *pos1, Position *pos2){
 Case c;

  for(c = A1; c <= H8; c++){
    if(est_case_occupee(pos1, c) == 1 && est_case_occupee(pos2, c) == 1){
      return 0;
    }
  }
  return 1;
}
