#include"headers/interface.h"

void ouvrir_fenetre(void){
  MLV_create_window("Echecs", "", 800, 600);
}

void effacer(void){
  MLV_draw_filled_rectangle(0, 0, 800, 600, MLV_COLOR_ALICE_BLUE);
   affiche_bouton_tour_precedent();
}

void afficher_position(Position *pos){
  int i, j;
  Case c = 0;
  char lettre[1], chiffre[1];

  for(j = 1; j <= 8; j++){
     lettre[0] = 'A' + j - 1;
     lettre[1] = '\0';   
     MLV_draw_text_box(100 + j * 60, 35,
		       60, 40,
		       lettre,
		       0,
		       MLV_COLOR_ALICE_BLUE, MLV_COLOR_BLACK, MLV_COLOR_ALICE_BLUE,
		       MLV_TEXT_CENTER,
		       MLV_HORIZONTAL_CENTER, MLV_VERTICAL_CENTER
		       );

     chiffre[0] = '1' + j - 1;
     chiffre[1] = '\0';
     MLV_draw_text_box(130, 7 + j * 60,
		       30, 60,
		       chiffre,
		       0,
		       MLV_COLOR_ALICE_BLUE, MLV_COLOR_BLACK, MLV_COLOR_ALICE_BLUE,
		       MLV_TEXT_CENTER,
		       MLV_HORIZONTAL_CENTER, MLV_VERTICAL_CENTER
		       );
  }

  for(i = 1; i <= 8; i++){
    for(j = 1; j <= 8; j++, c++){
      MLV_draw_rectangle(100 + j * 60, 7 + i * 60, 60, 60, MLV_COLOR_BLACK);
      if((j + i) % 2 == 0){
	MLV_draw_filled_rectangle(101 + j * 60, 8 + i * 60, 58, 58, MLV_COLOR_BROWN);
      }
      else{
	MLV_draw_filled_rectangle(101 + j * 60, 8 + i * 60, 58, 58, MLV_COLOR_WHITE);
      }
      if(est_case_occupee(pos, c) == 1){
	/*	MLV_draw_line(105 + j * 60, 12 + i * 60, 155 + j * 60, 62 + i * 60, MLV_COLOR_RED); 
		MLV_draw_line(155 + j * 60, 12 + i * 60, 105 + j * 60, 62 + i * 60, MLV_COLOR_RED);*/
	MLV_draw_filled_circle(129 + j * 60, 37 + i * 60, 25, MLV_COLOR_BLACK);
      }
    }
  }
}

Case case_selectionner(void){
  int mouse_x, mouse_y, i, test;
  Case c;

  do{
    test = 0;
    c = -1;
    MLV_wait_mouse(&mouse_x, &mouse_y);

    /*Si clique sur précédent, retourne -1.*/
    if( (mouse_x >= 675 && mouse_x <= 775) && (mouse_y >= 275 && mouse_y <= 325)){
      return c;
    }

    for(i = 1; i <= 8 || c < 0; i++){
      if((mouse_x > 100 + i * 60) && (mouse_x < 100 + (i + 1) * 60)){
	c = i;
	test++;
      }
    }
    if(test == 1){
      for(i = 1; i <= 8 || c < 0; i++){
	if((mouse_y > 7 + i * 60) && (mouse_y < 7 + (i + 1) * 60) ){
	  c = c + (i - 1) * 8 - 1;
	  test++;
	}
      }
    }
  }while(test != 2);

  return c;
}

void signal_gagnant(void){
  MLV_draw_text_box(200, 200,
		    400, 200,
		    "Vous avez gagné.",
		    0,
		    MLV_COLOR_BLACK, MLV_COLOR_BLACK, MLV_COLOR_ALICE_BLUE,
		    MLV_TEXT_CENTER,
		    MLV_HORIZONTAL_CENTER, MLV_VERTICAL_CENTER
		    );

  MLV_actualise_window();
  MLV_wait_seconds(3);
}

void signal_perdant(void){
  MLV_draw_text_box(200, 200,
		    400, 200,
		    "Vous avez perdu.",
		    0,
		    MLV_COLOR_BLACK, MLV_COLOR_BLACK, MLV_COLOR_ALICE_BLUE,
		    MLV_TEXT_CENTER,
		    MLV_HORIZONTAL_CENTER, MLV_VERTICAL_CENTER
		    );

  MLV_actualise_window();
  MLV_wait_seconds(3);
}

void averti_placement_illegal(void){
  MLV_draw_text_box(200, 555,
		    400, 40,
		    "Un des placements est illégal.",
		    0,
		    MLV_COLOR_RED, MLV_COLOR_RED, MLV_COLOR_ALICE_BLUE,
		    MLV_TEXT_CENTER,
		    MLV_HORIZONTAL_CENTER, MLV_VERTICAL_CENTER
		    );
}

void affiche_bouton_tour_precedent(void){
  MLV_draw_text_box(675, 275,
		    100, 50,
		    "Précédent",
		    0,
		    MLV_COLOR_BLACK, MLV_COLOR_BLACK, MLV_COLOR_ALICE_BLUE,
		    MLV_TEXT_CENTER,
		    MLV_HORIZONTAL_CENTER, MLV_VERTICAL_CENTER
		    );
}

void fermer_fenetre(void){
  MLV_free_window();
}
