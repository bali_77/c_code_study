#ifndef interface
#define interface

#include"position.h"
#include<stdio.h>
#include<MLV/MLV_all.h>

void ouvrir_fenetre(void);

void effacer(void);

void afficher_position(Position *pos);

Case case_selectionner(void);

void signal_gagnant(void);

void signal_perdant(void);

void averti_placement_illegal(void);

void affiche_bouton_tour_precedent(void);

void fermer_fenetre(void);

#endif
