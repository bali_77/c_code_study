#ifndef algo
#define algo

#include"position.h"

void fusion_position(Position *pos1, Position *pos2);

int est_sans_menace_mutuelle(Position *pos);

#endif
