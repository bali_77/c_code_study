#include"headers/position.h"

struct _position{
  int tab[8][8];
};

int est_case_occupee(Position *pos, Case c){
  if(pos->tab[c % 8][c / 8] == 1){
    return 1;
  }

  return 0;
}

Position* creer_position(){
  Position *pos;
  int i, j;

  if( (pos = (Position*) malloc(sizeof(struct _position))) == NULL){
    exit(1);
  }

  for(i = 0; i < 8; i++){
    for(j = 0; j < 8; j++){
      pos->tab[i][j] = 0;
    }
  }
  
  return pos;
}

void ajoute_position(Position *pos, Case c){
  pos->tab[c % 8][c / 8] = 1;
}

Position* calculer_cases_attaquees_dame(Case c){
  Position *pos = creer_position();
  Case c1, c2, c3, c4;
  int i;

  for(i=0, c1=c/8, c2=c%8, c3=c-(c%8)*9, c4=c+(c%8)*7; c1<=(c/8)+8; i++, c1++, c2=c2+7, c3=c3+9, c4=c4-7){
    if(c1 % 8 != c % 8){
      pos->tab[c1 % 8][c / 8] = 1;
    }
    if(c2 / 8 != c / 8){
      pos->tab[c % 8][c2 / 8] = 1;
    }
    if(c3 >= 0 && c3 < 64 && c3 % 8 != c % 8){
      /*Empêche de continuer la diagonale si elle a atteint un bord*/
      if(i != 0 && c3 % 8 < (c3 - 9) % 8){
	c3 = 64;
      }
      else{
	pos->tab[c3 % 8][c3 / 8] = 1;
      }
    }
    if(c4 >= 0 && c4 < 64 && c4 % 8 != c % 8){
      pos->tab[c4 % 8][c4 / 8] = 1;
      /*Empêche de continuer la diagonale si elle a atteint un bord*/
      if(i != 0 && c4 % 8 > (c4 - 7) % 8){
	c4 = -1;
      }
    }
  }

  return pos;
}

int compare_position(Position *pos1, Position *pos2){
  Case c;

  for(c = A1; c <= H8; c++){
    if(est_case_occupee(pos1, c) == 1 && est_case_occupee(pos2, c) == 1){
      return 0;
    }
  }
  return 1;
}
