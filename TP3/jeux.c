#include"headers/jeux.h"

void jeux_dames(void){
  Position *pos[8];
  int i, j, test;
  Case c;

  ouvrir_fenetre();
  effacer();

  pos[0] = creer_position();
  afficher_position(pos[0]);
  affiche_bouton_tour_precedent();

  for(i = 0; i < 8; ){
    MLV_actualise_window();
    /*Récupère la case et y ajoute une dame.*/
    do{
      test = 0;
      c = case_selectionner();
      if(c == -1){
	if(i > 0){
	  test = 2;
	}
      }
      else if(est_case_occupee(pos[i], c) == 0){
	test = 1;
      }
    }while(test == 0);

    /*Si n'a pas cliqué sur précédent.*/
    if(test == 1){
      ajoute_position(pos[i], c);

      /*Si deux dames se menaces, prévient le joueur.*/
      if(est_sans_menace_mutuelle(pos[i]) == 0){
	averti_placement_illegal();
      }
      i++;
      pos[i] = creer_position();
      fusion_position(pos[i], pos[i - 1]);
    }
    else{
      i--;
      free(pos[i]);
      pos[i] = creer_position();
      if(i > 0){
	fusion_position(pos[i], pos[i - 1]);
      }
      effacer();
      for(j = 0; j < i; j++){
	afficher_position(pos[j]);
      }
    }
    afficher_position(pos[i]);
  }


  if(est_sans_menace_mutuelle(pos[7]) == 1){
    signal_gagnant();
  }
  else{
    signal_perdant();
  }

  /*Libère la mémoire utilisée.*/
  for(i = 0; i < 8; i++){
    free(pos[i]);
  }
  fermer_fenetre();
}
