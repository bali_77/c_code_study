#include<stdio.h>

int question1(void){
  unsigned int x = 1;
  x = x << 14;
  return x;
}

int question2(void){
  unsigned int x = 1;
  x = ~(x << 13);
  return x;
}

void question3(unsigned int *x){
  unsigned int y = 1;
  y = y << 12;
  *x = *x | y;
}

void question4(unsigned int *x){
  unsigned int y = 0;
  y = y << 11;
  y = ~y;
  *x = *x & y;
}

int question5(unsigned int x){
  unsigned int y = 1;
  y = y << 10;
  y = y & x;
  return y;
}

int question6(unsigned int x, unsigned int y, int decallage){
  unsigned int z = 1;
  if(x & (z << decallage) == y & (z << decallage)){
      return 1;
  }
  else{
    return 0;
  }
}

int question7(unsigned int x, unsigned int y){
  int i;
  for(i = 0; i < 32; i++){
    if(question6(x, y, i) == 1){
      return 1;
    }
  }
  return 0;
}

int main(void){
  
}
