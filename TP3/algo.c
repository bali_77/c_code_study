#include"headers/algo.h"

void fusion_position(Position *pos1, Position *pos2){
  Case c;

  for(c = A1; c <= H8; c++){
    if(est_case_occupee(pos2, c) == 1){
      ajoute_position(pos1, c);  
    }
  }
}

int est_sans_menace_mutuelle(Position *pos){
  Position *menace = creer_position();
  Case dames[64], c;
  int i = 0, j;

  for(c = A1; c <= H8; c++){
    if(est_case_occupee(pos, c) == 1){
      dames[i] = c;
      i++;
    }
  }

  for(j = 0; j < i; j++){
    tab_cases_attaquees_dame[j] = calculer_cases_attaquees_dame(dames[j]);
    fusion_position(menace, tab_cases_attaquees_dame[j]);
    free(tab_cases_attaquees_dame[j]);
  }
  
  if(compare_position(pos, menace) == 0){
    free(menace);
    return 0;
  }

  free(menace);
  return 1;
}
