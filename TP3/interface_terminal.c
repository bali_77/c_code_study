#include<stdio.h>
#include"headers/interface.h"

void ouvrir_fenetre(void){
  /*Inutile pour le terminal.*/
}

void effacer(void){
  /*Inutile pour le terminal.*/
}

void afficher_position(Position *pos){
  Case c;
  int i;
  
  printf("  ");
  for(i = 'A'; i <= 'H'; i++){
    printf("%c ", i);
  }

  for(c = A1, i = 1; c <= H8; c++){
    if(c % 8 == 0){
      printf("\n");
      printf("%d ", i);
      i++;
    }
    if(est_case_occupee(pos, c) == 0){
      printf(". ");
    }
    else{
      printf("+ ");
    }
  }
  printf("\n\n");
}

Case case_selectionner(void){
  char case_c[2];
  Case c;

  do{
    c = -1;
    printf("Coordonnée: ");
    if( (scanf("%2s", case_c)) == 1){
      if(case_c[0] >= 'A' && case_c[0] <= 'H'){
	c = case_c[0] - 'A';
      }
      else if(case_c[0] >= 'a' && case_c[0] <= 'h'){
	c = case_c[0] - 'a';
      }
      if(case_c[1] - '0' >= 0 || case_c[1] - '0' <= 9){
	c = c + (case_c[1] - '0' - 1) * 8;
      }
    }
    printf("\n");
  }while(c < A1 || c > H8);

  return c;
}

void signal_gagnant(void){
  printf("Vous avez gagné.\n");
}

void signal_perdant(void){
  printf("Vous avez perdu.\n");
}

void averti_placement_illegal(void){
  printf("Un des placements est illégal.\n");
}

void affiche_bouton_tour_precedent(void){
  /*Inutile pour le terminal.*/
}

void fermer_fenetre(void){
  /*Inutile pour le terminal.*/
}
