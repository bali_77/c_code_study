#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <MLV/MLV_all.h>
#define WIDTH 800
#define HEIGHT 600

/* BUYUK BARAN / ANTOINE COURTIER / TP5PROG3 / TP DAME  RENDU LE 13/11/14
	gcc -o test exo2.c -lMLV 
 */

/** Structure etc ... */

typedef int INT32;
typedef unsigned int UINT32;
typedef unsigned long long UINT64;
typedef UINT64 Position;

Position tab_case_attaque[64];

typedef struct coord{

	int x;
	int y;
}Coord;

typedef enum place{
        A1,B1,C1,D1,E1,F1,G1,H1,
	A2,B2,C2,D2,E2,F2,G2,H2,
	A3,B3,C3,D3,E3,F3,G3,H3,
	A4,B4,C4,D4,E4,F4,G4,H4,
	A5,B5,C5,D5,E5,F5,G5,H5,
	A6,B6,C6,D6,E6,F6,G6,H6,
	A7,B7,C7,D7,E7,F7,G7,H7,
	A8,B8,C8,D8,E8,F8,G8,H8

}Case;

/****************************/

/* prototypes de fonctions */

/* Renvoie un int si la case est occupé ou pas */
int case_occupe(Position pos, Case c);
/* renvoie un UINT64 pour une case donnée */
Position placer_dame(Case c);
/* affiche la position dans le terminal */
void afficher_pos(Position c);
/* calcule par rapport à une case donne les cases attaquées */
Position calculer_cases_attaquees(Case c);
/* Renvoie 0 ou 1 si les dames se menacent  */
int est_sans_menace(Position pos);
/*lance le plateau sans les dames / bouton precedent / utilise la mlv*/
void mlv_lanceur();
/* Renvoie les pixels de la souris sous forme de tableau pour l'echequier / utilise la mlv*/
void mouse_motion(int *a , int *b);
/* vérifie s'il n'y a pas de pions deja present sur la case cliquée */
int legal_mouse(int coup , Position pos);
/* dessine un pion tout simplement */
void dessiner_pions(int x ,int y);
/* dessine le bouton precedent / utilise la mlv*/
void precedent();
/* alerte le joueur par des infosbulles / utilise la mlv*/
void alert(int a);
/* fin de partie ou pas / utilise la mlv*/
void fin_partie(int a);


/***************************/

/* fonctions ... ***************/


int case_occupe(Position pos, Case c){

	/* ex si on veut tester la case 12 
		donc on veut la 12 eme case à partir de la gauche 
			donc on decale 63-12 cases pour avoir la 12 eme 
				case en derniere position et la tester */

	return ((pos >> (63-c)) & 1)?1:0;

}

/* place une dame */
Position placer_dame(Case c){

	Position tmp=1ull;
	return tmp << (63-c);
}

/* fonctions affiichage ascci , aucune utilité ici */
void afficher_pos(Position c){
	
	int cpt,i;
		for(cpt=H8;cpt >= A1 ;cpt=cpt-8){
		 	printf("  ");
			for(i=7; i>=0; i--){
			      	if((c & (1ull <<(63-cpt+i)))>0)
					printf(" + ");
				else
					printf(" . "); 
				printf(" ");
			}
	         			printf("\n");
		}

		printf("\n\n");
}

/* renvoie un UINT64 par rapport à c */
Position calculer_cases_attaquees(Case c){
  Position pos=0;
  Case c1, c2, c3, c4;
  int i;

  for(i=0, c1=c-c%8, c2=c%8, c3=c-(c%8)*9, c4=c+(c%8)*7; i<=7; i++, c1++, c2=c2+
	8, c3=c3+9, c4=c4-7){
    if(c1 != c){
      pos= pos | (1ull<<c1);
    }
    if(c2 != c){
      pos=pos | (1ull<<c2);
    }
    if(c3 >= 0 && c3 < 64 && c3 != c){
      /*Empêche de continuer la diagonale si elle atteint un bord*/
      if(i != 0 && c3 % 8 < (c3 - 9) % 8){
	c3 = 64;
      }
      else{
	pos = pos | (1ull<<c3);
      }
    }
    if(c4 >= 0 && c4 < 64 && c4 != c){
      pos = pos | (1ull<<c4);
      /*Empêche de continuer la diagonale si elle atteint un bord*/
      if(i != 0 && c4 % 8 > (c4 - 7) % 8){
	c4 = -1;
      }
    }
  }

  return pos;
}

/* si les dames ne se menacent pas , renvoie 0 ou 1 */
int est_sans_menace(Position pos){

	Position menaces=0;
	int i=0,j=0;

	
	for(i=0; i<= 63 ; i++){

		/* on verifie si une dame se trouve sur la case */
		if( (pos & (1ull<<i)) > 0){
			/* on rentre toutes les menaces dans menaces sauf la dame utilisé */
				for(j=0;j<=63;j++){
				if( j != (63-i)){

					if( (pos & (1ull<<(63-j))) > 0){
					menaces = menaces | tab_case_attaque[j];
					}
				}
				}

				if( ( (1ull << (i)) & menaces) > 0){
					return 1;
				}


			menaces=0;
		}
	}

	return 0;	


}

/* dessine l'echequier */
void mlv_lanceur(){
	/* affiche dans pos graphiquement */
	int i,j;

	/* carre prend  80 de la largeur et 100 hauteur*/

	int carre_x= (WIDTH*0.80)/8;
	int carre_y= HEIGHT/8;
	int color=1;

	for(i=0; i< (WIDTH*0.80); i=i+carre_x){
		for(j=0; j < (HEIGHT) ; j=j+carre_y){
			if( color ==0){
			MLV_draw_filled_rectangle(i,j,carre_x,carre_y,MLV_COLOR_BLACK);
			color=1;
			}
			else{
				MLV_draw_filled_rectangle(i,j,carre_x,carre_y,MLV_COLOR_WHITE);
				color=0;

			}
			

		}
			if( color == 1)
				color=0;
			else
				color=1;

		}
		
		
	
	MLV_actualise_window();

}

/*renvoie la case de lechequier cliquer */
void mouse_motion(int *a , int *b){

	int c,d;
	int ok=0;

	while( ok == 0){
		
		MLV_wait_mouse(&c,&d);

		/* clic precedent ... */
		
		if( c > (WIDTH*0.81) && d > 0 && d < (HEIGHT*0.10)){
			*a=99;
			*b=99;
			return;
		}
		

		c= c/((WIDTH*0.80)/8);
		d= d/(HEIGHT/8);

		if( c >=0 && c < 8 && d >= 0 && d < 8){
			ok=1;
			*a=d;
			*b=c;
		}
		

	}


}

/* verifie si le coup est legal ***/

int legal_mouse(int coup , Position pos){

	if( coup < 0 || coup > 64){
		printf("Coup invalide \n");
		return 0;
	}
	if( (pos & ( 1ull << (63-coup))) > 0){
		printf("Pions present \n");
		return 0;
	}
	
	return 1;
}

/* dessiner les pions */

void dessiner_pions(int x ,int y){

	int w= WIDTH*0.80/8;
	int h= (HEIGHT/8);

	double c=0, d=0;
	int i;


	for(i=0; i<(y);i++)
		c=c+w;
	for (i = 0; i < (x); i++) {
		d=d+h;
	}
	
	MLV_Image *dame=NULL;

	dame = MLV_load_image("dame.png");

	if( dame == NULL){
		printf("Erreur chargement dame.png \n");
		exit(EXIT_FAILURE);
	}

	/**** adapte etc  ***/

	MLV_resize_image(dame,w,h);

	MLV_draw_image(dame, c, d);

	
	MLV_actualise_window();

	MLV_free_image(dame);

}


/* dessine le bouton precedent */
void precedent(){

	MLV_draw_adapted_text_box(WIDTH*0.81,0,"PRECEDENT",30,MLV_COLOR_BLUE,MLV_COLOR_BLUE,MLV_COLOR_BLACK,MLV_TEXT_CENTER);
	MLV_actualise_window();
}

/* previent si les dames placees sont bon ou pas */
void alert(int a){

	if( a == 1)
		MLV_draw_adapted_text_box(WIDTH*0.85,HEIGHT*0.40,"Attention\n mauvais \n placement ...",20,MLV_COLOR_BLUE, MLV_COLOR_BLUE,MLV_COLOR_BLACK , MLV_TEXT_CENTER);
	else if( a == 2)
		MLV_draw_adapted_text_box(WIDTH*0.85,HEIGHT*0.40,"Attention\n Nettoyage: \n ok ",20,MLV_COLOR_BLUE, MLV_COLOR_BLUE,MLV_COLOR_BLACK , MLV_TEXT_CENTER);
	else
		MLV_draw_adapted_text_box(WIDTH*0.85,HEIGHT*0.40,"Attention !! \n Non Rien \n :) ",20,MLV_COLOR_BLUE,MLV_COLOR_BLUE, MLV_COLOR_BLACK , MLV_TEXT_CENTER);

	MLV_actualise_window();
}

/* annonce si oui ou non on a gagné */

void fin_partie(int a){

	if( a == 0)
		MLV_draw_adapted_text_box(WIDTH/2,HEIGHT/2,"Vous avez gagné !!!",50,MLV_COLOR_BLUE,MLV_COLOR_BLUE,MLV_COLOR_BLACK,MLV_TEXT_CENTER);
	else
		MLV_draw_adapted_text_box(WIDTH/3,HEIGHT/3,"Vous avez perdu !!!",50,MLV_COLOR_BLUE,MLV_COLOR_BLUE,MLV_COLOR_BLACK,MLV_TEXT_CENTER);


	MLV_actualise_window();
	MLV_wait_seconds(3);
}

/***************************************/


/******************************************* DEBUT PROGRAMME *****************************************************************/

int main(int argc, const char *argv[])
{	
	int i,j,k,l;
	Position coup[8]={0,};
	Coord cd[8];
	int tab_equiv[8][8];
	int mx,my;
	int win=0;
	int ctn;


	/* initialiser le tableau des attaques  et le tableau des equivalences*/

	for(i=63-7,k=0; i>= 0; i=i-8,k++)
		for(j=0;j<8;j++)
			tab_equiv[k][j]=i+j;


	for(i=0;i<=63;i++)
		tab_case_attaque[i]=calculer_cases_attaquees(63-i);
	/***************************************/

	/* initialiser la fenetre */
	MLV_create_window("DAME","DAME",WIDTH,HEIGHT);
	MLV_draw_filled_rectangle(0,0,WIDTH,HEIGHT,MLV_COLOR_WHITE);
	MLV_actualise_window();
	mlv_lanceur();
	precedent();
	/*************************/
		
	/*lance le jeu */
	i=0;
	while( i< 8){
		
		/* C'est parti */
		do{
			mouse_motion(&mx,&my);
		/* PRECEDENT SI CLIQUER*/
			if( mx == 99 && my == 99 && i > 0){
				/* si le coup n'est pas le premier */
				mlv_lanceur();

				coup[i-1]=0;
				i--;
				
				if( i > 1){	
				for(l=0; l <= (i-1) ; l++){
					dessiner_pions(cd[l].x,cd[l].y);
				}
				}
				else if( i == 1)
					dessiner_pions(cd[0].x,cd[0].y);	
				ctn = 0;


			}/* FIN PRECEDENT */
			else if ( i == 0 && mx == 99 ){
				/* Si precedent est le premier coup */
				mlv_lanceur();
				coup[0]=0;
				ctn = 0;
			}
			else {				

				/* sinon on continue le jeu normalement */
				if( i > 0 && mx != 99)
					ctn=legal_mouse(tab_equiv[mx][my],coup[i-1]);
				else if ( mx != 99)
					ctn=1;
			}
		
				

			}while( ctn  == 0);


		/* on place la dame */
		if( i==0 ){
			coup[i]=placer_dame(tab_equiv[mx][my]);
		}
		else
			coup[i]=coup[i-1] | placer_dame(tab_equiv[mx][my]);
			
		/* dessine la dame */		
			dessiner_pions(mx,my);
			cd[i].x=mx;
			cd[i].y=my;	
		/* on previent le joueur s'il a bon ou pas */
		win=est_sans_menace(coup[i]);
		alert(win);
		
		printf("INFO ASCII COUP==> %d VALEUR==> %llu \n",i,coup[i]);
		i++;
	}
	
	/* on annnonce la fin de partie */
	fin_partie(win);
	printf("Fermeture Veuillez patienter !\n");
	MLV_wait_seconds(3);
	
	MLV_free_window();	
	return 0;
}
/**************************************************************FIN PROGRAMMME **************************************/

