#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* BUYUK BARAN / ANTOINE COURTIER / EXO1 / TP5PROG3 RENDU LE 13/11/14 */


/* BREF VOILA LE L'EXO 1 */
int question1(void){
  unsigned int x = 1;
  x = x << 14;
  return x;
}

int question2(void){
  unsigned int x = 1;
  x = ~(x << 13);
  return x;
}

void question3(unsigned int *x){
  unsigned int y = 1;
  y = y << 12;
  *x = *x | y;
}

void question4(unsigned int *x){
  unsigned int y = 0;
  y = y << 11;
  y = ~y;
  *x = *x & y;
}

int question5(unsigned int x){
  unsigned int y = 1;
  y = y << 10;
  y = y & x;
  return y;
}

int question6(unsigned int x, unsigned int y, int decallage){
  unsigned int z = 1;
  if(x & (z << decallage) == y & (z << decallage)){
      return 1;
  }
  else{
    return 0;
  }
}

int question7(unsigned int x, unsigned int y){
  int i;
  for(i = 0; i < 32; i++){
    if(question6(x, y, i) == 1){
      return 1;
    }
  }
  return 0;
}

/* FIN EXO1 */



int test_bit(int a,int b){

	int i,ok;

	for(i=0; i< 8; i++){
		if((a & ( 1 << i) & (b & ( 1<<i))))
			return 1;
	}

	
	return 0;
}

/* EXPLICATION ETC */
int main(int argc, const char *argv[])
{
	unsigned int test=0;
	/* entier avec le 15 eme bit à 1 c'est 2 = 0000 0000 0000 0010*/
	unsigned int a=2;
	/* entier dont le 14 eme bit est à 0 et les autres à 1 */
	unsigned int b=4; /* 0000 0000 0000 0100 */
	b= ~b; /* 1111 1111 1111 1011 avec la negation */
	/* treizieme bit à 1 seulement c quelquonque avec treizieme bit à 0
	c= 0000 0000 0000 0000 par exemple 
	 */
	unsigned int c=0;
	c=c | 8;
	/* 12 eme bit à 0 */
	unsigned int d=16; /* 0000 0000 0001 0000*/
	d= d & ~(16);  /* ~(16) = 1111 1111 1110 1111*/
	/* tester si le 11 eme bit est à 1  */ 
	printf("11 eme bit à 1 ? %d \n",( a & ( 1 << 11)));
	/* tester si le 10 eme bit de deux entiers */
	printf("%d des dixieme bits \n",(( a & (1 << 10) ) & (b & ( 1<< 10))));
	/* tester si les deux entiers ont au moins un bit à 1 ? */
        printf("%d \n",test_bit(a,test));


	return 0;
}
