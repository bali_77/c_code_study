#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <MLV/MLV_all.h>


/* BUYUK Baran && COURTIER Antoine 
	TP2 CHOMP M.LANDSCHOOT
	VERSION SANS SAUVEGARDE
	*/

/* structure et definition */

typedef struct{
	int **tab;
	int n;
	int m;
}Tablette;

typedef enum{
	 j1, j2
}Joueur;

typedef struct{
	Tablette position;
	Joueur joueur;
}Position;

typedef struct{
	int x;
	int y;
}Coup;

/* prototype etc ... */
int legal(Position *pos, Coup *coup);
/* si le coup est legal */
int termine(Position *pos, Joueur *jg);
/* si la partie est fini avec le dernier carré pris ... */
void convert(int *a , int *b , int n , int m , int width , int height);
/* convertie les coordonnees en x , y de la mlv en coordoonees normale*/
void draw_rectangle_tab(int n , int m , int width , int height,Tablette t);
/* dessine le plateau */
Tablette creer_tab(int n , int m);
/* creer le tableau et initialise le jeu */
void manger( Tablette *t , int x , int y);
/* mange le jeu */
void titre();
/* affiche le titre */
void info(Position partie,int a, int b,int c);
/* info sur le jeu */
void win(Position partie);
/* partie gagnée */
void info_mouse(int a , int b);
/* info souris */
void win2(Joueur a);
/* jeu gagné ou pas */

int main(int argc, const char *argv[])
{	
	int width=1280, height=720, n_x , n_y ,play=1, a ,b , leg, clic=0;
	MLV_Mouse_button mouse=-1;
	MLV_Button_state state=-1;
	MLV_Event event=MLV_NONE;
	Position partie;
	Coup coup;
	int j1s=0,j2s=0;
	int nb_partie;
	int cpt,cp2;	

	if( argc != 4){
		printf("Erreur arg invalide ... ( mini 3 arg  ==> taille_x_carré taille_y_carré max_partie)\n");
		exit(EXIT_FAILURE);
	}

	n_x=atoi(argv[1]);
	n_y=atoi(argv[2]);
	nb_partie=atoi(argv[3]);
	

	partie.position=creer_tab(n_x,n_y);
	partie.joueur=j2;
	

	MLV_create_window("CHOMP","ICONE",width,height);
	draw_rectangle_tab(n_x,n_y,width,height,partie.position);	
	titre();
	
	/* gooo , égalité sur le score est impossible */
	while( nb_partie > 0){ 
        /* boucle de demarrage */
	while( play == 1){
		
		
			
		if( partie.joueur == j1)
			partie.joueur=j2;
		else
			partie.joueur=j1;
		
		info(partie,j1s,j2s,nb_partie);
	
		/* boucle d'attente joueur */
		do{
			event=MLV_get_event(NULL,NULL,NULL,NULL,NULL,&a,&b,&mouse,&state);
			if( state == MLV_PRESSED)
				clic=1;
			else
				clic=0;
			convert(&a,&b,n_x,n_y,width,height);
			coup.x=a;
			coup.y=b;
			if( coup.x < 0 || coup.x > n_x || coup.y < 0 || coup.y > n_y || clic == 0){
				leg=0;
			}
			else if(coup.x < 0 || coup.x > n_x || coup.y < 0 || coup.y > n_y || clic == 1){
				leg=legal(&partie,&coup);
			}

			if( coup.x >= 0 && coup.x < n_x && coup.y >= 0 && coup.y < n_y)	    {
			info_mouse(a,b);
		}
		}while( leg != 1);
		
		manger(&(partie.position),a,b);

		play=termine(&partie,&(partie.joueur));

		draw_rectangle_tab(n_x,n_y,width,height,partie.position);

	}
	win(partie);
	/* reset */
	play=1;
	nb_partie--;

	/* on evite l'egalité */

	if( partie.joueur == j1)
		j2s++;
	else
		j1s++;

	partie.joueur=j2;

	if( nb_partie <= 0 && j2s == j1s)
		nb_partie++;

	
	for(cpt=0;cpt< n_x;cpt++)
		for(cp2=0;cp2<n_y;cp2++)
			partie.position.tab[cpt][cp2]=1;
	
	/* fin reset */
	draw_rectangle_tab(n_x,n_y,width,height,partie.position);	
	titre();

	}

	if( j1s > j2s)
		win2(j2);
	else
		win2(j1);
	

	MLV_free_window();

	return 0;
}

/* fonctions etc */

void info_mouse(int a , int b){
	int width=MLV_get_window_width();
	int height=MLV_get_window_height();
	char phrase[20]="";
	int deco=0;
	sprintf(phrase,"Coordonnees %d %d  ",a,b);
	MLV_draw_adapted_text_box( width/3,height/4,phrase,1, MLV_COLOR_BLUE, MLV_COLOR_BLUE,MLV_COLOR_BLACK, MLV_TEXT_CENTER);
	MLV_actualise_window();

}
void win2(Joueur a){
	int width=MLV_get_window_width();
	int height=MLV_get_window_height();
	int h,l;
	
	if(a == j1){
	MLV_draw_adapted_text_box( width/3,height/3,"Joueur 2 a gagné le jeu!!!!!",60, MLV_COLOR_BLUE, MLV_COLOR_BLUE,MLV_COLOR_BLACK, MLV_TEXT_CENTER);
	MLV_actualise_window();
	}
	else{
	MLV_draw_adapted_text_box( width/3,height/3,"Joueur 1 a gagné le jeu!!!!!",60, MLV_COLOR_BLUE, MLV_COLOR_BLUE,MLV_COLOR_BLACK, MLV_TEXT_CENTER);
	MLV_actualise_window();
	}

	MLV_wait_seconds(2);
	for(h=0; h< width; h=h+width/15)
		for(l=0; l< height; l= l+height/15){
			MLV_draw_filled_rectangle(h,l,width/15,height/15,MLV_COLOR_BLACK);	
			MLV_actualise_window();
			usleep(8000);
		}





}


void win(Position partie){
	int width=MLV_get_window_width();
	int height=MLV_get_window_height();
	int h,l;
	
	if(partie.joueur == j1){
	MLV_draw_adapted_text_box( width/3,height/3,"Joueur 2 a gagné !!!!!",60, MLV_COLOR_BLUE, MLV_COLOR_BLUE,MLV_COLOR_BLACK, MLV_TEXT_CENTER);
	MLV_actualise_window();
	}
	else{
	MLV_draw_adapted_text_box( width/3,height/3,"Joueur 1 a gagné !!!!!",60, MLV_COLOR_BLUE, MLV_COLOR_BLUE,MLV_COLOR_BLACK, MLV_TEXT_CENTER);
	MLV_actualise_window();
	}

	MLV_wait_seconds(2);
	for(h=0; h< width; h=h+width/15)
		for(l=0; l< height; l= l+height/15){
			MLV_draw_filled_rectangle(h,l,width/15,height/15,MLV_COLOR_BLACK);	
			MLV_actualise_window();
			usleep(8000);
		}





}
void titre(){

	int width=MLV_get_window_width();
	int height=MLV_get_window_height();
	MLV_draw_adapted_text_box( width/3,0,"!!!! CHOMP TP BUYUK COURTIER !!!!",1, MLV_COLOR_BLUE, MLV_COLOR_BLUE,MLV_COLOR_BLACK, MLV_TEXT_CENTER);
	MLV_actualise_window();

	}
void info(Position partie, int a , int b, int c){
	int width=MLV_get_window_width();
	int height=MLV_get_window_height();
	char tmp[15];
	char score[30];
	char part[20];
	sprintf(score,"Score ==> Joueur 1: %d // Joueur 2: %d \0",a,b);


	MLV_draw_adapted_text_box( width/3,height*0.05,score,1, MLV_COLOR_BLUE, MLV_COLOR_BLUE,MLV_COLOR_BLACK, MLV_TEXT_CENTER);


	if( partie.joueur == j1)
		strcpy(tmp,"Joueur 1 ==>  \0");
	else
		strcpy(tmp,"Joueur 2 ==>  \0");



	MLV_draw_adapted_text_box( width/3,height*0.12,tmp,1, MLV_COLOR_BLUE, MLV_COLOR_BLUE,MLV_COLOR_BLACK, MLV_TEXT_CENTER);
	
	sprintf(part,"Il reste %d parties ... \0",c);
	MLV_draw_adapted_text_box( width/3,height*0.15,part,1, MLV_COLOR_BLUE, MLV_COLOR_BLUE,MLV_COLOR_BLACK, MLV_TEXT_CENTER);
	

	MLV_actualise_window();



	
}
void draw_rectangle_tab(int n , int m , int width , int height, Tablette t){
	/* dessine les premiers rectangle */
	int a ,b ;

	/* le tableau se dessine sur un rectangle de 10% marge cote et 30 % hauteur de l'ecran */
	int d_c=width*0.90;
	int d_h=height*0.60;
	int dew=width*0.10;
	int deh=height*0.40;
	/* taille d'un carré */
	int l_c=d_c/n;
	int h_c=d_h/m;

	for(a=0; a<n; a++)
		for(b=0; b<m; b++){
			if( t.tab[a][b] == 1)
			MLV_draw_rectangle(dew+a*l_c,deh+b*h_c,l_c,h_c,MLV_COLOR_BLUE);
			else
				MLV_draw_filled_rectangle(dew+a*l_c,deh+b*h_c,l_c,h_c,MLV_COLOR_BLUE);

		}
		MLV_actualise_window();

}
void convert(int *a , int *b , int n , int m , int width , int height){
	/* convertie les donnees souris en case tableau precis */
	*a=(*a-width*0.10)/(width*0.90/n);
	*b=(*b-height*0.40)/(height*0.60/m);

}

Tablette creer_tab(int n , int m){
	
	Tablette tmp;

	tmp.tab=(int **)malloc(sizeof(int*)*n);

	assert(tmp.tab != NULL);

	int i,c;

	for(i=0; i<n; i++)
		tmp.tab[i]=calloc(m,sizeof(int));

	for(i=0; i<n; i++)
		for(c=0; c<m; c++){
			assert( tmp.tab[i] != NULL);
			tmp.tab[i][c]=1;
		}

	tmp.n=n;
	tmp.m=m;

	return tmp;
}

void manger( Tablette *t , int x , int y){

	int i,c;

	for(i=x; i< (*t).n ; i++)
		for(c=y; c<(*t).m; c++)
			(*t).tab[i][c]=0;
}

int legal(Position *pos, Coup *coup){
	return (*pos).position.tab[(*coup).x][(*coup).y]==1 && (*coup).x < (*pos).position.n && (*coup).y < (*pos).position.m?1:0;
}

int termine(Position *pos, Joueur *jg){
	return (*pos).position.tab[0][0]==1?1:0;
}

